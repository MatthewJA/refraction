from __future__ import division

import random

from sympy import symbols, S, Segment, Circle, intersection, cos, sin, Point, asin, N, Line, atan
from sympy import pi as PI

import draw

####################### CONSTANTS #######################

### Simulation properties ###

# Dimensions of the simulation.
WIDTH = HEIGHT = S(500)

# The smallest increment that a photon can move in the simulation. Roughly, the resolution of the simulation.
MINIMUM_STEP = S(1)#/S(5)

# The length of the simulation in steps.
TOTAL_STEPS = 700

### Geometry properties ###

# Properties of the circle.
RADIUS = S(100)
CENTRE = Point(WIDTH/2, HEIGHT/2)

# Properties of the light.
START_X = S(0)
START_Y = S(0)
START_D = S(9)/S(10)

# Number of particles in the starting beam of light.
PARTICLE_COUNT = 50

# Wavelength limits.
LOWER_WAVELENGTH = S(380) # Violet.
UPPER_WAVELENGTH = S(730) # Red.

### Physical properties ###

# Temperature
ZERO_DEGREES_CELCIUS_IN_KELVIN = S("273.15") # K
STANDARD_AMBIENT_TEMPERATURE = S("298.15")
AMBIENT_TEMPERATURE = STANDARD_AMBIENT_TEMPERATURE

# Used to calculate refractive index of water.
WATER_REFRACTION_COEFFICIENTS = (
	lambda t: S("1.3208") - S("1.2325e-5") * t - S("1.8674e-6") * (t**2) + S("5.0233e-9") * (t**3),
	lambda t: S("5208.2413") - S("0.5179") * t - S("2.284e-2") * (t**2) + S("6.9608e-5") * (t**3),
	lambda t: S("-2.5551e8") - S("18341.336") * t - S("917.2319") * (t**2) + S("2.7729") * (t**3),
	lambda t: S("9.3495") + S("1.7855e-3") * t + S("3.6733e-5") * (t**2) - S("1.2932e-7") * (t**3)
)

####################### HELPER FUNCTIONS #######################

def fRange(start, stop, step):
	i = start
	while i < stop:
		yield i
		i += step

def pointInCircle(point, circle):
	"""
	Return whether the given point is in the given circle.
	"""

	return (point[0]-CENTRE.x)**2 + (point[1]-CENTRE.y)**2 <= RADIUS**2

def refractiveIndexOfWater(wavelength):
	"""
	Refractive index of water as a function of wavelength.
	http://optics.sgu.ru/_media/optics/staff/bashkatov/bashkatov_spie_03_5068_393.pdf
	"""

	# Convert ambient temperature to Celcius.
	temp = AMBIENT_TEMPERATURE - ZERO_DEGREES_CELCIUS_IN_KELVIN

	n = (WATER_REFRACTION_COEFFICIENTS[0](temp) +
			WATER_REFRACTION_COEFFICIENTS[1](temp) / (wavelength**2) +
			WATER_REFRACTION_COEFFICIENTS[2](temp) / (wavelength**4) +
			WATER_REFRACTION_COEFFICIENTS[3](temp) / (wavelength**6))

	return n

def snell(i, n1, n2):
	"""
	Return the angle of refraction given the angle of incidence,
	in accordance with Snell's Law.
	"""

	print N(asin(n1 * sin(i) / n2), 50)
	return N(asin(n1 * sin(i) / n2), 50)

####################### CLASSES #######################

class GeometryError(Exception):
	"""
	Raised when something unexpected happens geometrically,
	for example, no intersections being found where we expect intersections.
	"""

	pass

class Photon(object):
	"""
	Represent light.
	Has direction and position, such that movement can be calculated for subsequent simulation steps.
	Has wavelength.
	"""

	def __init__(self, x, y, d, w):
		self.x = x # x position of the photon.
		self.y = y # y position of the photon.
		self.d = d # Direction of the photon, from the positive x axis.

		self.w = w # Wavelength of the photon.

		self.lastX = x # Previous x position.
		self.lastY = y # Previous y position.
		self.wasInCircle = False # Whether the photon was in the circle last step.

	def step(self, circle):
		"""
		Simulate this particle.
		"""

		# Freeze the current position as the last position.
		self.lastX = self.x
		self.lastY = self.y

		# Move.
		self.x += MINIMUM_STEP * cos(self.d)
		self.y += MINIMUM_STEP * sin(self.d)

		# Have we passed the circle boundary?
		# If so, refract.
		nowInCircle = pointInCircle((self.x, self.y), circle)
		if nowInCircle and not self.wasInCircle:
			# We have entered the circle.
			self.wasInCircle = True
			self.refract(circle, entering=True)
		elif not nowInCircle and self.wasInCircle:
			self.wasInCircle = False
			self.refract(circle, entering=False)

	def refract(self, circle, entering):
		"""
		Change the direction of the photon in accordance with Snell's Law.
		"""

		# We've crossed the circle boundary, so we need to backtrack.
		
		# Where did we hit?
		# We first construct a segment through which we travelled this step.
		self.x = N(self.x)
		self.y = N(self.y)
		self.lastX = N(self.lastX)
		self.lastY = N(self.lastY)

		lastPath = Segment((self.x, self.y), (self.lastX, self.lastY))

		# Then we can find where we collided with the circle.
		points = circle.intersection(lastPath)

		if len(points) == 1:
			# This is ideally what we will get.
			collisionPoint = points[0]
		elif len(points) == 2:
			# We intersected twice, so our resolution is too low to accurately refract.
			# This is a problem: If we refract, we might miss the next collision (by jumping over it).
			# It's better, in this case, to ignore the intersections entirely and just assume we were
			# tangent to the shape instead.
			return

		else:
			# We haven't actually intersected at all.
			# This should technically never happen, but according to SymPy's documentation,
			# it might happen. Best to just raise an error rather than do something strange.
			raise GeometryError("No intersections found for segment that should cross circle.")

		# How far back was that point?
		dist = collisionPoint.distance((self.x, self.y))

		# Move to that point.
		self.x = collisionPoint.x
		self.y = collisionPoint.y

		# Now rotate based on Snell's Law.
		self.rotateBasedOnSnellsLaw(circle, entering)

		# Finally, move forward the distance we had moved already in the wrong direction.
		self.x += dist * cos(self.d)
		self.y += dist * sin(self.d)

		# Check whether we're in the circle, to make sure refraction doesn't fail on the next step and raise a GeometryError.
		self.wasInCircle = pointInCircle((self.x, self.y), circle)

	def hasNoticablyMoved(self):
		"""
		Return whether the particle has noticably moved.
		"Noticably" in this case refers to the actual display: If we've changed what pixel the light is in, then we have noticably moved.
		"""
		return int(self.x.n()) != int(self.lastX.n()) or int(self.y.n()) != int(self.lastY.n()) # int floors the position values.

	def getApparentLocation(self):
		"""
		Returns the tuple of floored coordinates for outputting.
		"""
		return (int(self.x.n()), int(self.y.n()))

	def rotateBasedOnSnellsLaw(self, circle, entering):
		if entering:
			n1 = 1
			n2 = refractiveIndexOfWater(self.w)
		else:
			n1 = refractiveIndexOfWater(self.w)
			n2 = 1

		tangentSlope = -(self.x-circle.center.x)/(self.y-circle.center.y)

		if tangentSlope == S("+inf"):
			tangentAngle = PI/2
		else:
			tangentAngle = atan(tangentSlope)

		# Rotate angles to fit these axes
		print "regular d is ", N(self.d, 50)
		print "tangent angle is", N(tangentAngle, 50)
		self.d -= tangentAngle
		print "normalised d is", N(self.d, 50)

		self.d = self.d % (2*PI)

		# Refract.
		if PI/2 <= self.d < 3*PI/2:
			# Entering the first quadrant. Or the fourth. Same kind of guessing as below.
			self.d = PI/2 + snell(self.d - PI/2, n1, n2)
		elif 3*PI/2 <= self.d < 2*PI or 0 <= self.d < PI/2:
			# Entering the second quadrant. Or third quadrant - I'm taking a guess here that it's going to be the same formula.
			self.d = PI/2 - snell(PI/2 - self.d, n1, n2)
		else:
			self.d = 0

		# Rotate back.
		self.d += tangentAngle

		self.d = N(self.d, 20)

####################### MAIN #######################

def main():
	circle = Circle(CENTRE, RADIUS)

	light = set()
	for w in fRange(LOWER_WAVELENGTH, UPPER_WAVELENGTH, (UPPER_WAVELENGTH-LOWER_WAVELENGTH)/PARTICLE_COUNT):
		light.add(Photon(START_X, START_Y, START_D, w))

	positions = {} # All drawable positions, for output. Contains wavelengths.

	# Simulate.
	for step in xrange(TOTAL_STEPS):
		for l in light:
			l.step(circle)

			# Add the light's position to positions, if we've moved from last time.
			if l.hasNoticablyMoved():
				apparentLocation = l.getApparentLocation()
				wavelengthsAtPosition = positions.get(apparentLocation, [])
				wavelengthsAtPosition.append(l.w.n()) # Floats make it easier to deal with in Pygame later.
				positions[apparentLocation] = wavelengthsAtPosition

		if random.random() < 0.1:
			print "{:.2%}".format(step/TOTAL_STEPS)

	draw.drawSimulation(positions, circle)
main()