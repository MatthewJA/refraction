INTENSITY = 0.1

def wavelengthToRGB(wavelength):
	"""
	Wavelength to RGB colour. Wavelength in nm.
	Uses Bruton's Algorithm.

	http://www.physics.sfasu.edu/astro/color/spectra.html
	http://stackoverflow.com/questions/3407942/rgb-values-of-visible-spectrum
	"""

	if wavelength >= 380 and wavelength < 440:
		R = -(wavelength - 440) / (440 - 380)
		G = 0.0
		B = 1.0
	elif wavelength >= 440 and wavelength < 490:
		R = 0.0
		G = (wavelength - 440) / (490 - 440)
		B = 1.0
	elif wavelength >= 490 and wavelength < 510:
		R = 0.0
		G = 1.0
		B = -(wavelength - 510) / (510 - 490)
	elif wavelength >= 510 and wavelength < 580:
		R = (wavelength - 510) / (580 - 510)
		G = 1.0
		B = 0.0
	elif wavelength >= 580 and wavelength < 645:
		R = 1.0
		G = -(wavelength - 640) / (640 - 580)
		B = 0.0
	elif wavelength >= 645 and wavelength <= 780:
		R = 1.0
		G = 0.0
		B = 0.0
	else:
		R = 0.0
		G = 0.0
		B = 0.0

	return (R * 255, G * 255, B * 255)

def drawSimulation(positions, circle, width=500, height=500):
	import sys

	import pygame
	from pygame.locals import QUIT

	screen = pygame.display.set_mode((width, height))

	# Draw light to screen.

	for x, y in positions:
		r = 0 # Red.
		g = 0 # Green.
		b = 0 # Blue.

		# Draw origin.
		pygame.draw.circle(screen, (255, 255, 255), (0, 0), 3)

		# Draw circle.
		cx = circle.center.x
		cy = circle.center.y
		cr = circle.radius
		pygame.draw.circle(screen, (255, 255, 255), (cx, cy), int(cr), 1)

		# Draw light.
		for wavelength in positions[(x, y)]:
			# Additive colours. The more wavelengths here, the brighter it gets.
			wR, wG, wB = wavelengthToRGB(wavelength)
			r = max(0, min(255, (r + wR * INTENSITY)))
			g = max(0, min(255, (g + wG * INTENSITY)))
			b = max(0, min(255, (b + wB * INTENSITY)))

		try:
			screen.set_at((x, y), (r, g, b))
		except IndexError:
			pass

	pygame.image.save(screen, "lastOutput.png")

	while True:
		for e in pygame.event.get():
			if e.type == QUIT:
				pygame.quit()
				sys.exit()

		pygame.display.flip()